# ueditor-spring-boot-stater

#### 文件导入
+ 新建springboot项目
+ 不需要下载本项目，jar包已上传到中央仓库
```xml?linenums
	<dependency>
	    <groupId>cn.jasonone.ueditor</groupId>
	    <artifactId>ueditor-spring-boot-starter</artifactId>
	    <version>1.2.2</version>
	</dependency>
```
### 版本历史
#### 1.1.3
+ 1.修复自定义文件持久化对象不生效问题[#IXCR4](https://gitee.com/hmjasonone/ueditor-spring-boot-stater/issues/IXCR4)
+ 2.修复错误:Unable to read meta-data for class
#### 1.2.0 __该版本不向下兼容__
+ 1.使用全新的持久化接口(cn.jasonone.upload.UeditorStorate)代替cn.jasonone.upload.UeditorUpload接口
+ 2.简化自定义持久化接口的定义
+ 3.修改启用方式为使用@EnableUeditor注解启用
+ 4.修复缺少配置文件时无提示问题[#I12KGP](https://gitee.com/hmjasonone/ueditor-spring-boot-stater/issues/I12KGP)
+ 5.自定义文件持久化对象配置方式变更
+ + 1.2.0之前 
在application.yml中配置
+ + 1.2.0
在@EnableUeditor注解中配置
#### 1.2.1
+ 1.支持图片根地址配置
+ 2.支持后端配置文件路径配置
#### 1.2.2
+ 1.修复使用默认rootPath配置报错的问题 [#I1GO02](https://gitee.com/hmjasonone/ueditor-spring-boot-stater/issues/I1GO02)
#### 教程
[使用教程](https://blog.csdn.net/xujie784442670/article/details/84533380)
#### 示例
[ueditor-spring-boot-example](https://gitee.com/hmjasonone/ueditor-spring-boot-example)