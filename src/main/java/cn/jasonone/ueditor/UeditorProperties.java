package cn.jasonone.ueditor;

import cn.jasonone.ueditor.exception.ConfigException;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.util.ResourceUtils;
import org.springframework.util.StringUtils;

import javax.annotation.PostConstruct;
import java.io.FileNotFoundException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * 配置属性
 *
 * @author Jason
 */
@Component
@Scope(ConfigurableBeanFactory.SCOPE_SINGLETON)
@ConfigurationProperties("ue")
@Primary
@Data
@Slf4j
public class UeditorProperties {
	private static final String CONFIG_FILE_NAME = "config.json";
	private static final String DEFAULT_SERVER_URL = "/ueditor/jsp/controller";
	private static final String DEFAULT_ROOT_PATH = "classpath:/static";
	/**
	 * 上传文件存放目录
	 */
	private String rootPath = DEFAULT_ROOT_PATH;
	/**
	 * 统一服务接口名称配置,config.json存放目录为服务接口的上级目录 例: rootPath: classpath:/static
	 * serverUrl: /ueditor/controller 则config.json存放目录为:
	 * classpath:/static/ueditor/config.json
	 */
	private String serverUrl = DEFAULT_SERVER_URL;
	/**
	 * 后端配置文件路径: 默认为 rootPath+serverUrl+"/config.json"
	 */
	private String configFilePath;
	/**
	 * 图片访问根路径
	 */
	private String baseImageUrl = "";
	
	@PostConstruct
	public void init() {
		checkServerUrl();
		checkRootPath();
		checkConfig();
	}
	
	/**
	 * 检测统一服务接口名称
	 */
	private void checkServerUrl() {
		if (StringUtils.isEmpty(this.serverUrl)) {
			this.serverUrl = DEFAULT_SERVER_URL;// 如果配置为空,则设置为默认值
			log.warn("统一服务接口名称配置为空,启用默认配置[{}]", DEFAULT_SERVER_URL);
		}
	}
	
	/**
	 * 检测后端配置
	 */
	private void checkConfig() {
		Path path=null;
		if (this.configFilePath == null || this.configFilePath.isEmpty()) {
			//如果没有配置具体的配置文件路径,则使用默认规则寻找配置文件
			path = Paths.get(this.rootPath,this.serverUrl);
			path = path.getParent();
		}else if (StringUtils.startsWithIgnoreCase(this.configFilePath, "classpath:")) {
			this.configFilePath = this.configFilePath.trim().replace("\\", "/");
			path = Paths.get(this.rootPath,this.configFilePath.substring(10));
		}else{
			path = Paths.get(this.configFilePath);
		}
		
		if(Files.isDirectory(path)){
			path = path.resolve(CONFIG_FILE_NAME);
		}
		
		if (!Files.exists(path)) {
			log.error("找不到后端配置文件:{}", path);
			throw new ConfigException("找不到后端配置文件:" + path);
		}
	}
	
	/**
	 * 检测并转换目录配置
	 */
	private void checkRootPath() {
		if (StringUtils.isEmpty(this.rootPath)) {
			this.rootPath = DEFAULT_ROOT_PATH;
			log.warn("上传文件存放目录配置为空,启用默认配置[{}]", DEFAULT_ROOT_PATH);
		}
		
		if (StringUtils.startsWithIgnoreCase(this.rootPath, "classpath:")) {
			this.rootPath = this.rootPath.replace("\\", "/");
			String path = this.rootPath.substring(10);
			try {
				path = ResourceUtils.getFile("classpath:") + path;
				this.rootPath = path;
			} catch (FileNotFoundException e) {
				log.error("上传目录配置错误:{}", this.rootPath);
				throw new ConfigException("上传目录配置错误:" + this.rootPath, e);
			}
		}
	}
}
