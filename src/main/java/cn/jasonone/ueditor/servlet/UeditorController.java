package cn.jasonone.ueditor.servlet;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.context.ApplicationContext;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.stereotype.Component;
import org.springframework.util.ResourceUtils;
import org.springframework.util.StringUtils;

import cn.jasonone.ueditor.ActionEnter;
import cn.jasonone.ueditor.EnableUeditor;
import cn.jasonone.ueditor.UeditorProperties;
import cn.jasonone.ueditor.storage.LocationFileStorage;
import cn.jasonone.ueditor.upload.StorageManager;
import cn.jasonone.ueditor.upload.UeditorStorage;
import lombok.extern.slf4j.Slf4j;
/**
 * 统一服务入口
 * @author Jason
 *
 */
@Slf4j
@Component("cn.jasonone.UeditorController")
public class UeditorController extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Resource
	private UeditorProperties properties;
	private String rootPath;
	private UeditorStorage upload;
	@Resource
	private ApplicationContext applicationContext;

	@Override
	protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		StorageManager storageManager = new StorageManager(upload);
		storageManager.setProperties(properties);
		resp.getWriter().write(new ActionEnter(storageManager, req, rootPath).exec());
	}

	private String paserPath(String path) throws FileNotFoundException {
		if (StringUtils.isEmpty(path)) {
			return path;
		}
		if (StringUtils.startsWithIgnoreCase(path, "classpath:")) {
			path = path.substring(10);
			return ResourceUtils.getFile("classpath:") + path;
		}
		return path;
	}

	@Override
	public void init() throws ServletException {
		rootPath = properties.getRootPath();
		try {
			rootPath = paserPath(rootPath);
		} catch (FileNotFoundException e) {
			log.error(e.getMessage(), e);
		}
		log.info("Ueditor root-path:[{}]", rootPath);
		this.upload = getStorage();
		log.info("Ueditor File Storage:[{}]", this.upload.getClass().getName());
	}

	private UeditorStorage getStorage() {
		Class<? extends UeditorStorage> type = null;
		{
			//获取启动注解配置的Storage配置类
			Map<String, Object> beansWithAnnotation = applicationContext.getBeansWithAnnotation(EnableUeditor.class);
			for (Object obj : beansWithAnnotation.values()) {
				EnableUeditor ueditor = AnnotationUtils.findAnnotation(obj.getClass(), EnableUeditor.class);
				Class<? extends UeditorStorage> defaultValue = ueditor.value();
				if (defaultValue != null) {
					type = defaultValue;
					break;
				}
			}
		}
		//如果配置为null,则启用默认配置
		if (type == null) {
			type = LocationFileStorage.class;
		}
		upload = applicationContext.getBean(type);
		return upload;
	}
}
