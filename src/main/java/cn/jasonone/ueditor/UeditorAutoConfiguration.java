package cn.jasonone.ueditor;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import cn.jasonone.ueditor.servlet.UeditorController;
import lombok.extern.slf4j.Slf4j;
/**
 * 自动配置类
 * @author Jason
 *
 */
@Slf4j
@Configuration
@EnableConfigurationProperties(UeditorProperties.class)
@ComponentScan("cn.jasonone.ueditor")
public class UeditorAutoConfiguration {
	@Autowired
	private UeditorProperties properties;
	@Resource
	private UeditorController uc;

	@Bean
	public ServletRegistrationBean<UeditorController> ueditorController() {
		ServletRegistrationBean<UeditorController> srb = new ServletRegistrationBean<>(uc);
		log.info("Ueditor Server Url: [{}]", properties.getServerUrl());
		srb.addUrlMappings(properties.getServerUrl());
		return srb;
	}
}
