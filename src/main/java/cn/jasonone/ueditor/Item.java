package cn.jasonone.ueditor;

import java.io.File;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
/**
 * 封装文件对象
 * @author Jason
 *
 */
@NoArgsConstructor
@Data
@Accessors(chain = true)
public class Item {
	private String path;

	public Item(File file) {
		setPath(file);
	}

	public Item(String path) {
		setPath(path);
	}

	public Item(Path path) {
		setPath(path);
	}

	public Item setPath(File file) {
		this.path = file.getAbsolutePath();
		return this;
	}

	public Item setPath(Path path) {
		this.path = path.toString();
		return this;
	}

	public Item setPath(String path) {
		this.path = path;
		return this;
	}

	public static List<Item> asList(File... files) {
		return asList(Arrays.asList(files));
	}

	public static List<Item> asList(Collection<File> files) {
		if (files.isEmpty()) {
			return Collections.emptyList();
		}
		List<Item> list = new ArrayList<>();
		files.forEach(f -> list.add(new Item(f)));
		return list;
	}

	public String getSuffix() {
		return path.substring(path.lastIndexOf('.'));
	}

}
