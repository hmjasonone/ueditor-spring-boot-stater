package cn.jasonone.ueditor;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.core.annotation.AliasFor;

import cn.jasonone.ueditor.storage.LocationFileStorage;
import cn.jasonone.ueditor.upload.UeditorStorage;
/**
 * ueditor自动配置注解
 * @author Jason
 *
 */
@Retention(RUNTIME)
@Target(TYPE)
@Inherited
@ImportAutoConfiguration(value = UeditorAutoConfiguration.class)
public @interface EnableUeditor {
	@AliasFor("value")
	Class<? extends UeditorStorage> storage() default LocationFileStorage.class;

	@AliasFor("storage")
	Class<? extends UeditorStorage> value() default LocationFileStorage.class;
}
