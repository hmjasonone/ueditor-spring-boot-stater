package cn.jasonone.ueditor.exception;
/**
 * 统一异常封装类
 * @author Jason
 *
 */
public class UeditorException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public UeditorException() {
		super();
	}

	public UeditorException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public UeditorException(String message, Throwable cause) {
		super(message, cause);
	}

	public UeditorException(String message) {
		super(message);
	}

	public UeditorException(Throwable cause) {
		super(cause);
	}

}
