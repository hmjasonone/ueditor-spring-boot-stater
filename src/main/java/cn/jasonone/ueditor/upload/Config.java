package cn.jasonone.ueditor.upload;

import java.util.Map;
/**
 * 配置接口
 * @author Jason
 *
 */
public interface Config {
	/**
	 * 设置配置对象
	 * @param conf 配置集合
	 */
	public void setConfig(Map<String,Object> conf);
}
