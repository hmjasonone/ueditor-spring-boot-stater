package cn.jasonone.ueditor.upload;

import java.io.File;
import java.io.IOException;
import java.util.Map;

import com.baidu.ueditor.define.BaseState;

/**
 * 文件持久化接口
 * 
 * @author Jason
 * @since 1.1.5
 */
public interface UeditorStorage extends FileManager, Config {

	/**
	 * 检测用于对于path是否拥有写权限
	 * 
	 * @param savePath 保存路径
	 * @return 处理结果
	 */
	public BaseState validFile(String savePath);

	/**
	 * 持久化文件信息
	 * 
	 * @param file     临时文件
	 * @param rootPath 文件保存根目录
	 * @param path     文件保存路径(相对于根目录)
	 * @throws IOException 文件处理出错
	 */
	public void save(File file, String rootPath, String path) throws IOException;

	/**
	 * 持久化文件信息
	 * 
	 * @param data     文件数据
	 * @param rootPath 文件保存根目录
	 * @param savePath 文件保存目录(相对于根目录)
	 * @throws IOException 文件处理出错
	 */
	public void save(byte[] data, String rootPath, String savePath) throws IOException;

	/**
	 * 设置文件保存根目录
	 * 
	 * @param rootPath 文件保存根目录
	 */
	public void setRootPath(String rootPath);

	/**
	 * 设置获取到哪个目录读取文件列表
	 * 
	 * @param dir 目录
	 */
	public void setDir(String dir);

	/**
	 * 设置读取文件列表的文件数量
	 * 
	 * @param count 文件数量
	 */
	public void setCount(Integer count);

	/**
	 * 设置需要过滤的文件后缀
	 * 
	 * @param allowFiles 后缀列表
	 */
	public void setAllowFiles(String[] allowFiles);

	/**
	 * 如需重写此方法必须调用{@link UeditorStorage#setConfig(Map)},否则将导致以下属性无法取值:
	 * <ul>
	 * <li>rootPath</li>
	 * <li>dir</li>
	 * <li>count</li>
	 * <li>allowFiles</li>
	 * </ul>
	 */
	@Override
	default void setConfig(Map<String, Object> conf) {
		setRootPath((String) conf.get("rootPath"));
		setDir((String) conf.get("dir"));
		setCount((Integer) conf.get("count"));
		setAllowFiles((String[]) conf.get("allowFiles"));
	}

}
