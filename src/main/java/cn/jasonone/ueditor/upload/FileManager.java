package cn.jasonone.ueditor.upload;

import java.util.Arrays;
import java.util.Collection;

import com.baidu.ueditor.PathFormat;
import com.baidu.ueditor.define.BaseState;
import com.baidu.ueditor.define.MultiState;
import com.baidu.ueditor.define.State;

import cn.jasonone.ueditor.Item;
/**
 * 文件管理接口
 * @author Jason
 *
 */
public interface FileManager {
	/**
	 * 获取文件列表
	 * 
	 * @param index 文件索引
	 * @return 返回处理结果
	 */
	public State listFile(int index);

	/**
	 * 获得状态集合
	 * 
	 * @param rootPath 文件保存根路径
	 * @param items    文件列表
	 * @return 状态集合
	 */
	default MultiState getState(String rootPath, Item... items) {
		return getState(rootPath, Arrays.asList(items));
	}

	/**
	 * 获得状态集合
	 * 
	 * @param rootPath 文件保存根路径
	 * @param items    文件列表
	 * @return 状态集合
	 */
	default MultiState getState(String rootPath, Collection<Item> items) {
		MultiState state = new MultiState(true);
		BaseState fileState = null;
		for (Item item : items) {
			if (item == null) {
				break;
			}
			fileState = new BaseState(true);
			fileState.putInfo("url", this.getPath(rootPath, PathFormat.format(item.getPath())));
			state.addState(fileState);
		}
		return state;
	}

	/**
	 * 将文件路径转换为URL访问路径
	 * 
	 * @param rootPath 文件保存根路径
	 * @param filePath 文件绝对路径
	 * @return 文件的URL访问路径
	 */
	default String getPath(String rootPath, String filePath) {
		return filePath.replace(rootPath, "");
	}

	/**
	 * 获取文件后缀集合
	 * 
	 * @param fileExt 文件后缀数组
	 * @return 文件后缀数组
	 */
	default String[] getAllowFiles(String[] fileExt) {
		String[] exts = null;
		String ext = null;
		if (fileExt == null) {
			return new String[0];
		}
		exts = fileExt;
		for (int i = 0, len = exts.length; i < len; i++) {
			ext = exts[i];
			exts[i] = ext.replace(".", "");
		}
		return exts;

	}
}
